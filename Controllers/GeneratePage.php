<?
class GeneratePage extends \Framework\Controllers\ACss {
	/* Life cycle methods */

	public function Initialize() {
		$this->Model->DefaultFontSize = $_GET['dfs'] ?? '16px';

		$this->Model->FontSizes = explode(',', $_GET['fs'] ?? '0.75rem,0.8125rem,0.875rem,0.9375rem,1rem,1.125rem,1.25rem,1.375rem,1.5rem');

		if(count($this->Model->FontSizes) !== 9) {
			die('Exactly 9 font sizes must be specified.');
		}

		$this->Model->ParagraphHeight = 1.25;

		$this->Model->ParagraphMargin = ($this->Model->ParagraphHeight - 1.0) / 2.0;

		$this->Model->Scopes = [
			'ss',
			'fs'
		];

		$this->Model->Sizes = [
			'ss' => [
				'xxs' => [
					'Value' => 360,
					'Postfix' => 'px'
				],
				'xs' => [
					'Value' => 480,
					'Postfix' => 'px'
				],
				's' => [
					'Value' => 800,
					'Postfix' => 'px'
				],
				'm' => [
					'Value' => 1024,
					'Postfix' => 'px'
				],
				'l' => [
					'Value' => 1280,
					'Postfix' => 'px'
				],
				'xl' => [
					'Value' => 1440,
					'Postfix' => 'px'
				],
				'xxl' => [
					'Value' => 1600,
					'Postfix' => 'px'
				],
				'xxxl' => [
					'Value' => 1920,
					'Postfix' => 'px'
				]
			],
			'fs' => [
				'xs' => [
					'Value' => 0.25,
					'Postfix' => 'rem'
				],
				's' => [
					'Value' => 0.5,
					'Postfix' => 'rem'
				],
				'm' => [
					'Value' => 1,
					'Postfix' => 'rem'
				],
				'l' => [
					'Value' => 2,
					'Postfix' => 'rem'
				],
				'xl' => [
					'Value' => 4,
					'Postfix' => 'rem'
				]
			]
		];

		$this->Initialize»Screens();

		parent::Initialize();
	}

	public function Initialize»Screens() {
		$this->Model->Screens = [
			[
				'Index' => 0,
				'Variant' => 'min',
				'Prefix' => 'xxxsMin',
				'MinWidth' => null,
				'MaxWidth' => null
			],
			[
				'Index' => 0,
				'Variant' => null,
				'Prefix' => 'xxxs',
				'MinWidth' => null,
				'MaxWidth' => 359
			],
			[
				'Index' => 0,
				'Variant' => 'max',
				'Prefix' => 'xxxsMax',
				'MinWidth' => null,
				'MaxWidth' => 359
			],
			[
				'Index' => 1,
				'Variant' => 'min',
				'Prefix' => 'xxsMin',
				'MinWidth' => 360,
				'MaxWidth' => null
			],
			[
				'Index' => 1,
				'Variant' => null,
				'Prefix' => 'xxs',
				'MinWidth' => 360,
				'MaxWidth' => 479
			],
			[
				'Index' => 1,
				'Variant' => 'max',
				'Prefix' => 'xxsMax',
				'MinWidth' => null,
				'MaxWidth' => 479
			],
			[
				'Index' => 2,
				'Variant' => 'min',
				'Prefix' => 'xsMin',
				'MinWidth' => 480,
				'MaxWidth' => null
			],
			[
				'Index' => 2,
				'Variant' => null,
				'Prefix' => 'xs',
				'MinWidth' => 480,
				'MaxWidth' => 799
			],
			[
				'Index' => 2,
				'Variant' => 'max',
				'Prefix' => 'xsMax',
				'MinWidth' => null,
				'MaxWidth' => 799
			],
			[
				'Index' => 3,
				'Variant' => 'min',
				'Prefix' => 'sMin',
				'MinWidth' => 800,
				'MaxWidth' => null
			],
			[
				'Index' => 3,
				'Variant' => null,
				'Prefix' => 's',
				'MinWidth' => 800,
				'MaxWidth' => 1023
			],
			[
				'Index' => 3,
				'Variant' => 'max',
				'Prefix' => 'sMax',
				'MinWidth' => null,
				'MaxWidth' => 1023
			],
			[
				'Index' => 4,
				'Variant' => 'min',
				'Prefix' => 'mMin',
				'MinWidth' => 1024,
				'MaxWidth' => null
			],
			[
				'Index' => 4,
				'Variant' => null,
				'Prefix' => 'm',
				'MinWidth' => 1024,
				'MaxWidth' => 1279
			],
			[
				'Index' => 4,
				'Variant' => 'max',
				'Prefix' => 'mMax',
				'MinWidth' => null,
				'MaxWidth' => 1279
			],
			[
				'Index' => 5,
				'Variant' => 'min',
				'Prefix' => 'lMin',
				'MinWidth' => 1280,
				'MaxWidth' => null
			],
			[
				'Index' => 5,
				'Variant' => null,
				'Prefix' => 'l',
				'MinWidth' => 1280,
				'MaxWidth' => 1439
			],
			[
				'Index' => 5,
				'Variant' => 'max',
				'Prefix' => 'lMax',
				'MinWidth' => null,
				'MaxWidth' => 1439
			],
			[
				'Index' => 6,
				'Variant' => 'min',
				'Prefix' => 'xlMin',
				'MinWidth' => 1440,
				'MaxWidth' => null
			],
			[
				'Index' => 6,
				'Variant' => null,
				'Prefix' => 'xl',
				'MinWidth' => 1440,
				'MaxWidth' => 1599
			],
			[
				'Index' => 6,
				'Variant' => 'max',
				'Prefix' => 'xlMax',
				'MinWidth' => null,
				'MaxWidth' => 1599
			],
			[
				'Index' => 7,
				'Variant' => 'min',
				'Prefix' => 'xxlMin',
				'MinWidth' => 1600,
				'MaxWidth' => null
			],
			[
				'Index' => 7,
				'Variant' => null,
				'Prefix' => 'xxl',
				'MinWidth' => 1600,
				'MaxWidth' => 1919
			],
			[
				'Index' => 7,
				'Variant' => 'max',
				'Prefix' => 'xxlMax',
				'MinWidth' => null,
				'MaxWidth' => 1919
			],
			[
				'Index' => 8,
				'Variant' => 'min',
				'Prefix' => 'xxxlMin',
				'MinWidth' => 1920,
				'MaxWidth' => null
			],
			[
				'Index' => 8,
				'Variant' => null,
				'Prefix' => 'xxxl',
				'MinWidth' => 1920,
				'MaxWidth' => null
			],
			[
				'Index' => 8,
				'Variant' => 'max',
				'Prefix' => 'xxxlMax',
				'MinWidth' => null,
				'MaxWidth' => null
			]
		];
	}

	public function Load() {
		foreach($this->Model->Screens as $i => $screen) {
			$this->CloneControl('FontSize', 'FontSize.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('TextAlign', 'TextAlign.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('Size', 'Size.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('Row', 'Row.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('Column', 'Column.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('Flex', 'Flex.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('Visibility', 'Visibility.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('Transition', 'Transition.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('ContentMargin', 'ContentMargin.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('ContentPadding', 'ContentPadding.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('Margin', 'Margin.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);

			$this->CloneControl('Padding', 'Padding.'.$i, ['Model' => ['Screens' => $this->Model->Screens, 'Screen' => $screen, 'Prefix' => $screen['Prefix']]]);
		}

		parent::Load();
	}
}
?>