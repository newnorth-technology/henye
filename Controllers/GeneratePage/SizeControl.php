<?
namespace GeneratePage;

class SizeControl extends \Framework\Controllers\ACss {
	/* Life cycle methods */

	public function Initialize() {
		$this->Model->Columns = 12;

		$this->Model->Sizes = [
			'ss' => [
				['Key' => 'xxxs', 'Value' => '240px'],
				['Key' => 'xxs', 'Value' => '360px'],
				['Key' => 'xs', 'Value' => '480px'],
				['Key' => 's', 'Value' => '800px'],
				['Key' => 'm', 'Value' => '1024px'],
				['Key' => 'l', 'Value' => '1280px'],
				['Key' => 'xl', 'Value' => '1440px'],
				['Key' => 'xxl', 'Value' => '1600px'],
				['Key' => 'xxxl', 'Value' => '1920px'],
			],
			'fs' => [
				['Key' => 'xxxs', 'Value' => $this->Owner->Model->FontSizes[0]],
				['Key' => 'xxs', 'Value' => $this->Owner->Model->FontSizes[1]],
				['Key' => 'xs', 'Value' => $this->Owner->Model->FontSizes[2]],
				['Key' => 's', 'Value' => $this->Owner->Model->FontSizes[3]],
				['Key' => 'm', 'Value' => $this->Owner->Model->FontSizes[4]],
				['Key' => 'l', 'Value' => $this->Owner->Model->FontSizes[5]],
				['Key' => 'xl', 'Value' => $this->Owner->Model->FontSizes[6]],
				['Key' => 'xxl', 'Value' => $this->Owner->Model->FontSizes[7]],
				['Key' => 'xxxl', 'Value' => $this->Owner->Model->FontSizes[8]],
			]
		];

		parent::Initialize();
	}
}
?>