<?
namespace GeneratePage;

class TextAlignControl extends \Framework\Controllers\ACss {
	/* Life cycle methods */

	public function Initialize() {
		$this->Model->Alignments = [
			'l' => 'left',
			'c' => 'center',
			'r' => 'right',
			'j' => 'justify'
		];

		parent::Initialize();
	}
}
?>