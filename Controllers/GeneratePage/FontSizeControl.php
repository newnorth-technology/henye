<?
namespace GeneratePage;

class FontSizeControl extends \Framework\Controllers\ACss {
	/* Life cycle methods */

	public function Initialize() {
		$this->Model->Sizes = [
			['Key' => 'xxxs', 'Value' => $this->Owner->Model->FontSizes[0]],
			['Key' => 'xxs', 'Value' => $this->Owner->Model->FontSizes[1]],
			['Key' => 'xs', 'Value' => $this->Owner->Model->FontSizes[2]],
			['Key' => 's', 'Value' => $this->Owner->Model->FontSizes[3]],
			['Key' => 'm', 'Value' => $this->Owner->Model->FontSizes[4]],
			['Key' => 'l', 'Value' => $this->Owner->Model->FontSizes[5]],
			['Key' => 'xl', 'Value' => $this->Owner->Model->FontSizes[6]],
			['Key' => 'xxl', 'Value' => $this->Owner->Model->FontSizes[7]],
			['Key' => 'xxxl', 'Value' => $this->Owner->Model->FontSizes[8]],
		];

		parent::Initialize();
	}
}
?>