<?
namespace GeneratePage;

class TransitionControl extends \Framework\Controllers\ACss {
	/* Life cycle methods */

	public function Initialize() {
		$this->Model->Attributes = [
			'mar' => 'margin',
			'pad' => 'padding',
			'bg' => 'background',
			'w' => 'width',
			'h' => 'height',
		];

		parent::Initialize();
	}
}
?>