<?
namespace GeneratePage;

class ContentMarginControl extends \Framework\Controllers\ACss {
	/* Life cycle methods */

	public function Initialize() {
		$this->Model->Sizes = [
			'a' => null,
			'n' => 0,
			'xxxs' => 0.0625,
			'xxs' => 0.125,
			'xs' => 0.25,
			's' => 0.5,
			'm' => 1,
			'l' => 2,
			'xl' => 4,
			'xxl' => 8,
			'xxxl' => 16
		];

		$this->Model->ParagraphLineHeight = 1.25;

		parent::Initialize();
	}
}
?>