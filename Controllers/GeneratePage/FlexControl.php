<?
namespace GeneratePage;

class FlexControl extends \Framework\Controllers\ACss {
	/* Life cycle methods */

	public function Initialize() {
		$this->Model->Wrap = [
			'nw' => 'nowrap',
			'w' => 'wrap',
			'rw' => 'wrap-reverse'
		];

		$this->Model->HAlign = [
			'fs' => 'flex-start',
			'fe' => 'flex-end',
			'c' => 'center',
			'sa' => 'space-around',
			'sb' => 'space-between'
		];

		$this->Model->VAlign = [
			'fs' => ['flex-start', 'flex-start'],
			'fe' => ['flex-end', 'flex-end'],
			'c' => ['center', 'center'],
			'sa' => ['space-around', 'stretch'],
			'sb' => ['space-between', 'stretch'],
			's' => ['stretch', 'stretch']
		];

		$this->Model->IAlign = [
			'fs' => 'flex-start',
			'fe' => 'flex-end',
			'c' => 'center',
			'b' => 'baseline',
			's' => 'stretch'
		];

		$this->Model->Columns = 12;

		$this->Model->Orders = 12;

		parent::Initialize();
	}
}
?>