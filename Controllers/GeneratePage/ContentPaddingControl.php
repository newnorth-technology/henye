<?
namespace GeneratePage;

class ContentPaddingControl extends \Framework\Controllers\ACss {
	/* Life cycle methods */

	public function Initialize() {
		$this->Model->Sizes = [
			'n' => 0,
			'xxxs' => 0.0625,
			'xxs' => 0.125,
			'xs' => 0.25,
			's' => 0.5,
			'm' => 1,
			'l' => 2,
			'xl' => 4,
			'xxl' => 8,
			'xxxl' => 16
		];

		parent::Initialize();
	}
}
?>