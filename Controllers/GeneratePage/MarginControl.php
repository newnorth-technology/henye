<?
namespace GeneratePage;

class MarginControl extends \Framework\Controllers\ACss {
	/* Life cycle methods */

	public function Initialize() {
		$this->Model->Sizes = [
			'a' => null,
			'n' => 0,
			'xs' => 0.25,
			's' => 0.5,
			'm' => 1,
			'l' => 2,
			'xl' => 4
		];

		parent::Initialize();
	}
}
?>